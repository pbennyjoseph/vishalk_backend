# syntax=docker/dockerfile:1
FROM python:3
# EXPOSE 8000
# CMD ["uwsgi", "--http", ":8000", "--ini", "./uwsgi/uwsgi.ini"]
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/