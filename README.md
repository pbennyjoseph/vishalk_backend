# Random Numbers Backend

## A task for `Vishal Kapoor`

Random Numbers Backend is made out of Django Rest Framework and PostgreSQL

- Database Design [design](https://dbdiagram.io/d/60e5bc137e498c3bb3ec2c8c)
- Live Demo (frontend) [demo](http://yoi.rf.gd/vishalk/vishalk_frontend)
- (not working) Live Backend (AWS EC2) [demo](http://54.147.123.44:8000/api/v1/post?format=json)
- Live Backend (Azure) [demo] (http://20.37.253.10/api/v1/post)


Note: The above live demos may not be available sometimes. please contact jbenny37@gmail.com

## Features

- Rate Limiting (10/minute) for post requests
- Dockerfile for easy EC2 deployment
- 90%+ Code Coverage
- Simple Database Design
- Swagger Documentation

## Tech

Random Numbers Backend uses a number of open source projects to work properly:

- [Django Rest Framework] - The REST framework for API
- [drf_yasg] - swagger documentation
- [python3] - The Python Language
- [postgresql_psycopg2] - PostgreSQL adapter for python

And of course Random Numbers Backend itself is open source with a [public repository](https://gitlab.com/pbennyjoseph/vishalk_backend) on GitLab.

## Installation

### Manual Installation

Use this for development / Debugging

Random Numbers Backend requires python3 to run.

- Install Python3, PostgreSQL and start the PostgreSQL server
- Create an empty database.
- Install `pipenv` using `pip3` for managing virtual environments
- Run `pipenv shell` at the project root to activate the virtual environment
- Run `pipenv install` at the project root to install all required dependencies
- Create a new file `local_settings.py` inside `backend/` folder.
- Make sure you edit all the variables in `backend/local_settings.py` appropriately.
  - DJANGO_SUPERUSER_EMAIL
  - DJANGO_SUPERUSER_USERNAME
  - DJANGO_SUPERUSER_PASSWORD
  - DATABASE_USER
  - DATABASE_PASSWORD
  - DATABASE_HOST
  - DATABASE_PORT
- Create a super user (optional)
  - Run `./manage.py createsuperuser --no-input`
- Migrate the database using `./manage.py migrate`
- Start the server using `./manage.py runserver`

Overall flow for development environments

```sh
pipenv shell
pipenv install
touch local_settings.py
# edit local variables
./manage.py createsuperuser --no-input
./manage.py migrate
./manage.py runserver
```

## Using Docker

For production environments. Clone the repository and run the two statements
Install docker using

```sh
sudo apt-get install docker-compose
```

```sh
sudo docker-compose build
sudo docker-compose run web python3 manage.py migrate
sudo docker-compose up
```

## Testing and Reports

Run the below command after manual installation to test and generate html reports in `./htmlcov` directory

```sh
coverage run --omit='*/virtual*' ./manage.py test && coverage html
```

## License

**Free Software, Hell Yeah!**

## References

[Django Rest Framework](https://www.django-rest-framework.org/)
[Production Deployment](https://dragonprogrammer.com/django-drf-api-production-docker/)
[Markdown Editor](https://dillinger.io/)
[Code Quality](https://docs.codeclimate.com/docs/list-of-engines)
[GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
