from django.db.models.base import Model
from api.utils import IPv4_REGEX
from django.db import models
from django.core.validators import RegexValidator

class RandomNumber(models.Model):
    """
    stores random numbers in a JSON field
    """
    numbers = models.JSONField()
    ip_address = models.CharField(validators=[
        RegexValidator(regex=IPv4_REGEX, message="Provide valid IP address.")], max_length=15)
    time = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-time', )
        constraints = [
            models.UniqueConstraint(fields=['ip_address', 'time'], name='rate limit')
        ]
