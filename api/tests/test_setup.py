from rest_framework.test import APITestCase, APIClient


class TestSetUp(APITestCase):
    def setUp(self):
        self.url = 'create-nums'
        self.client = APIClient(enforce_csrf_checks=True)
        return super().setUp()

    def tearDown(self):
        return super().tearDown()
