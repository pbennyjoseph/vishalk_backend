from api.tests.test_setup import TestSetUp
from django.urls import reverse


class TestViews(TestSetUp):

    def test_create_num_get(self):
        res = self.client.get(reverse(self.url))
        self.assertEqual(res.status_code, 200)

    def test_create_num_post(self):
        res = self.client.post(reverse(self.url))
        self.assertEqual(res.status_code, 201)
        res = self.client.get(reverse(self.url))
        self.assertEqual(len(res.data['results']), 1)

    def test_timeout(self):
        for i in range(10):
            res = self.client.post(reverse(self.url))
            self.assertEqual(res.status_code, 201)
        res = self.client.post(reverse(self.url))
        self.assertEqual(res.status_code, 429)