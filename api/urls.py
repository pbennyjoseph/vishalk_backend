from random import Random
from django.urls import path
from .views import *

urlpatterns = [
    path('post', RandomNumberListCreate.as_view(), name="create-nums"),
]
