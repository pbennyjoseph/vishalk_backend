from typing import OrderedDict
from django.http import request
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from api.serializers import *
from backend.settings import LO_VAL, HI_VAL, COUNT_NUMS
from random import randint
from django.utils import timezone


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class RandomNumberListCreate(generics.ListCreateAPIView):
    queryset = RandomNumber.objects.all()
    serializer_class = RandomNumberSerializer

    def post(self, request, *args, **kwargs):
        numbers = [randint(LO_VAL, HI_VAL) for _ in range(COUNT_NUMS)]
        request.data['ip_address'] = get_client_ip(request)
        request.data['numbers'] = numbers
        return self.create(request, *args, **kwargs)
