#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from backend.local_settings import *

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SUPERUSER_EMAIL',
                          DJANGO_SUPERUSER_EMAIL)
    os.environ.setdefault('DJANGO_SUPERUSER_USERNAME',
                          DJANGO_SUPERUSER_USERNAME)
    os.environ.setdefault('DJANGO_SUPERUSER_PASSWORD',
                          DJANGO_SUPERUSER_PASSWORD)
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
